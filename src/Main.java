import java.util.*;

public class Main {
    public static void main(String[] args) {
        Map <String, String> map = new HashMap<>();
        map.put("3", "три");
        map.put("5", "пять");
        map.put("2", "два");
        map.put("4", "четыре");
        map.put("1", "один");
        map.put("22", "один");
        map.put("40", "40");
        map.put("20", "один");
        map.put("30", "четыре");

        Map<Boolean, Float> map2 = new HashMap<>();
        map2.put(true, 2.5F);
        map2.put(false, 2.5F);

        System.out.println(map);
        System.out.println(reverseMap(map));
        System.out.println(map2);
        System.out.println(reverseMap(map2));
    }

    private static <K, V> Map<Collection<V>,K> reverseMap(Map<K, V> map){
        ArrayList<V> repeatedValues = new ArrayList<>(); //список для повторяющихся значений
        ArrayList<Collection<V>> soloItems = new ArrayList<>(); //список для одиночных (унарных) значений, которые уже повторились
        Map<Collection<V>, K> newMap = new HashMap<>(); //пустой map для return

        ArrayList<V> reversedValues = new ArrayList<>(map.values());
        Collections.reverse(reversedValues);
        //перевернутый список значений исходного map для сравнения элементов друг с другом (на повторения)


        for (V itemA : map.values()) { //вложенный цикл для поиска одинаковых элементов
            reversedValues.remove(reversedValues.size()-1);
            //после прохождения 1 итерации сокращается длина для избежания ошибочных повторений
            for (V itemZ : reversedValues) {
                //System.out.println(itemA + " " + itemZ);

                Optional<K> result = map.entrySet() //Нахождение ключа по значению
                        .stream()
                        .filter(entry -> itemA.equals(entry.getValue()))
                        .map(Map.Entry::getKey)
                        .findFirst();

                Collection<V> itemReserved = null; //Для замены одного массива ключей на другой
                if(itemA.equals(itemZ) && !newMap.isEmpty()){ //если значения одинаковы, то...
                    for (Collection<V> item: newMap.keySet()) {
                        //Вложенный цикл для новой Map для нахождения уже имеющихся повторяющихся ключей в массиве
                        for (V itemNext: item) {
                            if(itemNext.equals(itemA)){
                                //если такой находится, то "резервируется" ключ (массив) и новый ключ будет включать в себя
                                //значения в массиве из старого ключа плюс очередное повторение
                                itemReserved = item;
                                repeatedValues.addAll(item);
                                repeatedValues.add(itemNext);
                                break;
                            }
                        }
                    }
                    newMap.remove(itemReserved); //удаление старого ключа
                    soloItems.add(itemReserved); //добавление унарного элемента для удаления в будущем
                }
                else{ //если повторяющихся нет, просто записывается ключ
                    repeatedValues.add(itemA);
                }
                newMap.put(repeatedValues, result.get());
                repeatedValues = new ArrayList<>(); //обнуление списка
            }
        }
        for (Collection<V> item: soloItems) { //удаление таких унарненьких лишненьких элементиков
            if(newMap.containsKey(item)){
                newMap.remove(item);
            }
        }
        return newMap;
    }
}
